<%-- 
    Document   : listarAutores
    Created on : 13/03/2017, 20:27:15
    Author     : juliano.vieira
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Listar todos Autores</title>
</head>
<body>
    <p><a href="Controller?action=edit_autor">Cadastrar novo autor</a></p>
    <p><a href="Controller?action=search_autor">Buscar autor</a></p>
    
    <div>        
        <form method="GET" action='Controller' name="formBuscarAutores">        
            Nome do autor : <input type="text" name="nome" value="" />             
            <input type="submit" value="Buscar" />
            <input type="hidden" value="search_autor" name="action" />
        </form>        
    </div>
    
    <hr>
    
    <table border=0>
        <thead>
            <tr>
                <th>Codigo</th>
                <th>Nome</th>
                
                <th colspan=2>Ação</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${autores}" var="autor">
                <tr>
                    <td><c:out value="${autor.codigo}" /></td>
                    <td><c:out value="${autor.nome}" /></td>                    
                    <td><a href="Controller?action=edit_autor&codigo=<c:out value="${autor.codigo}"/>">Editar</a></td>
                    <td><a href="Controller?action=delete_autor&codigo=<c:out value="${autor.codigo}"/>">Deletar</a></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    
</body>
</html>