<%-- 
    Document   : listarLivros
    Created on : 13/03/2017, 20:27:15
    Author     : juliano.vieira
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Listar todos Livros</title>
</head>
<body>
    <p><a href="Controller?action=edit_livro">Cadastrar novo livro</a></p>
    <p><a href="Controller?action=search_livro">Buscar livro</a></p>
    
    <div>        
        <form method="GET" action='Controller' name="formBuscarLivros">        
            Nome do livro : <input type="text" name="titulo" value="" />             
            <input type="submit" value="Buscar" />
            <input type="hidden" value="search_livro" name="action" />
        </form>        
    </div>
    
    <hr>
    
    <table border=0>
        <thead>
            <tr>
                <th>ISBN</th>
                <th>Título</th>
                
                <th colspan=2>Ação</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${livros}" var="livro">
                <tr>
                    <td><c:out value="${livro.isbn}" /></td>
                    <td><c:out value="${livro.titulo}" /></td>                    
                    <td><a href="Controller?action=edit_livro&isbn=<c:out value="${livro.isbn}"/>">Editar</a></td>
                    <td><a href="Controller?action=delete_livro&codigo=<c:out value="${livro.isbn}"/>">Deletar</a></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    
</body>
</html>