<%-- 
    Document   : livro
    Created on : 21/02/2017, 20:35:13
    Author     : juliano.vieira
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css"
            href="css/ui-lightness/jquery-ui-1.8.18.custom.css" rel="stylesheet" />
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>
            
        <title>Adicionar livro</title>
    </head>
<body>
    
    <form method="POST" action='Controller' name="formAdicionarLivro">
        
        ISBN : <input type="text" readonly="readonly" name="isbn"
            value="<c:out value="${livro.isbn}" />" /> <br /> 
        
        Título : <input
            type="text" name="nome"
            value="<c:out value="${livro.titulo}" />" /> <br /> 
        
                <input type="submit" value="Cadastrar" />
    </form>
</body>
</html>