/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import bean.Autor;
import bean.Livro;
import fabricaDeSessoes.HibernateUtil;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author juliano.vieira
 */
public class DAOAutor {
    
    // DAO = Data Access Object
    // Usa os métodos de SessionFactory
        
    static SessionFactory fabrica = HibernateUtil.getSessionFactory();
    
    public void addAutor(String nome){               
        Autor autor = new Autor();
        autor.setNome(nome);
        
        Session sessao = fabrica.openSession();
        sessao.beginTransaction();
        sessao.save(autor);
        sessao.getTransaction().commit();
        sessao.close();
    }
    
    
    public Autor buscaAutor(String nome){
        Session sessao = fabrica.openSession();
        sessao.beginTransaction();
        Query q = sessao.createQuery("from Autor where nome = :nome");
        q.setParameter("nome", nome);
        sessao.close();
        return (Autor) q.uniqueResult();        
    }
    
    public Autor getAutorPeloCodigo(int codigo){
        Session sessao = fabrica.openSession();
        sessao.beginTransaction();
        Query q = sessao.createQuery("from Autor where codigo = :codigo");
        q.setParameter("codigo", codigo);
        sessao.close();
        return (Autor) q.uniqueResult();
    }
    
    public Autor getAutorPeloNome(String nome){
        Session sessao = fabrica.openSession();
        sessao.beginTransaction();
        Query q = sessao.createQuery("from Autor where nome = :nome");
        q.setParameter("nome", nome);
        sessao.close();
        return (Autor) q.uniqueResult();
    }
    
    public static List getAutores() {
        Session sessao = fabrica.openSession();
        sessao.beginTransaction();
        Query q = sessao.createQuery("from Autor");
        List list = q.list();
        sessao.close();
        return list;
    }    
    
    
    public void deletarAutor(int codigo){
        Session sessao = fabrica.openSession();
        sessao.beginTransaction();
        Query q = sessao.createQuery("delete from Autor where codigo = :codigo");
        q.setParameter("codigo", codigo);
        sessao.close();
    }    
    
    public void alterarAutor(Autor autor){
        Session sessao = fabrica.openSession();
        sessao.beginTransaction();
        Query q = sessao.createQuery("update Autor set nome = :nome where codigo = :codigo");
        q.setParameter("nome", autor.getNome());
        q.setParameter("codigo", autor.getCodigo());
        sessao.close();
    }
        
   public  void adicionarLivro(Autor autor, String isb){
         DAOLivro daoLivro = new DAOLivro();
        
        Livro livro = daoLivro.buscarLivro(isb);
        ArrayList<Livro> livros;
        if(autor.getListaDeLivros()==null){
             livros = new ArrayList<Livro>();
            livros.add(livro);   
        }else{
            livros=(ArrayList<Livro>) autor.getListaDeLivros();
            livros.add(livro); 
        }
        
        autor.setListaDeLivros(livros);
        Session sessao = fabrica.openSession();
        sessao.beginTransaction();
        sessao.persist(autor);
        sessao.getTransaction().commit();
        sessao.close();
   }
    
    
    
}
