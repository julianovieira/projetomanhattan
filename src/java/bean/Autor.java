/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 *
 * @author juliano.vieira
 */

// classe normal DTO + notation (incluir mapping no hibernate)
// <mapping class="bean.NomeDaClasse"/>

@Entity
@Table(name="autor")
public class Autor {
    
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private int codigo;    
    @Column(name="Nome")
    private String nome;
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "livro_autor", 
             joinColumns = { @JoinColumn(name = "codigo") }, 
             inverseJoinColumns = { @JoinColumn(name = "isbn") })
    private List<Livro> listaDeLivros;
     
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

   
    public List<Livro> getListaDeLivros() {
        return listaDeLivros;
    }

    /**
     * @param listaDeLivros the listaDeLivros to set
     */
    public void setListaDeLivros(List<Livro> listaDeLivros) {
        this.listaDeLivros = listaDeLivros;
    }
       
}
