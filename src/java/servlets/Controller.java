/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import dao.DAOAutor;
import dao.DAOLivro;
import bean.Autor;
import bean.Livro;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author juliano.vieira
 */

 @WebServlet(name = "Controller", urlPatterns = {"/Controller"})

public class Controller extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
 
    private static final String INSERIR_EDITAR_LIVRO = "/livro.jsp";
    private static final String INSERIR_EDITAR_AUTOR = "/autor.jsp";
    private static final String LISTAR_LIVRO = "/listarLivros.jsp";
    private static final String LISTAR_AUTOR = "/listarAutores.jsp";
    private static final String BUSCAR_LIVRO = "/buscarLivros.jsp";
    private static final String BUSCAR_AUTOR = "/buscarAutores.jsp";
    private final DAOLivro daoLivro;
    private final DAOAutor daoAutor;

    public Controller() {
        super();
        daoLivro = new DAOLivro();
        daoAutor = new DAOAutor();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        String forward = "";
        
        /* INÍCIO DA DIFUSÃO */
        
        String action = request.getParameter("action");

        /* DELETAR : autor */        
        if (action.equalsIgnoreCase("delete_autor")){       
        
            int codigo = Integer.parseInt(request.getParameter("codigo"));
            daoAutor.deletarAutor(codigo);
            forward = LISTAR_AUTOR;
            request.setAttribute("autores", DAOAutor.getAutores());
                
        /* DELETAR : livro */            
        } else if (action.equalsIgnoreCase("delete_livro")){            
            
            
            String isbn = request.getParameter("isbn");
            daoLivro.deletarLivro(isbn);
            forward = LISTAR_LIVRO;
            request.setAttribute("livros", DAOLivro.getLivros());
            
            
        /* EDITAR : autor */        
        } else if (action.equalsIgnoreCase("edit_autor")){            
            
            forward = INSERIR_EDITAR_AUTOR;
            int codigo = Integer.parseInt(request.getParameter("codigo"));
            Autor autor = daoAutor.getAutorPeloCodigo(codigo);
            request.setAttribute("autores", autor);
            
        /* EDITAR : livro */        
        } else if (action.equalsIgnoreCase("edit_livro")){
            
            forward = INSERIR_EDITAR_LIVRO;
            String isbn = request.getParameter("isbn");
            Livro livro = daoLivro.getLivroPeloISBN(isbn);
            request.setAttribute("livros", livro);
            
        /* PROCURAR : autor */            
        } else if (action.equalsIgnoreCase("search_autor")){
            forward = BUSCAR_AUTOR;
            String nome = request.getParameter("nome");            
            request.setAttribute("autores", daoAutor.getAutorPeloNome(nome));
        
        /* PROCURAR : livro */            
        } else if (action.equalsIgnoreCase("search_livro")){
            forward = BUSCAR_LIVRO;
            String titulo = request.getParameter("titulo");
            request.setAttribute("livros", daoLivro.getLivroPeloTitulo(titulo));
        
        /* LISTAR : autor */            
        } else if (action.equalsIgnoreCase("list_autor")){           
            forward = LISTAR_AUTOR;                                  
            request.setAttribute("autores", DAOAutor.getAutores());
            
        /* LISTAR : livro */            
        } else if (action.equalsIgnoreCase("list_livro")){            
            forward = LISTAR_LIVRO;
            request.setAttribute("livros", DAOLivro.getLivros());            
            
        } else {            
            forward = LISTAR_AUTOR;
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Autor autor = new Autor();
        autor.setNome(request.getParameter("nome"));
        String codigo = request.getParameter("codigo");
        if(codigo == null || codigo.isEmpty())
        {
            daoAutor.addAutor(autor.getNome());
        }
        else
        {
            autor.setCodigo(Integer.parseInt(codigo));            
            daoAutor.alterarAutor(autor);
        }
        RequestDispatcher view = request.getRequestDispatcher(LISTAR_AUTOR);
        request.setAttribute("autores", DAOAutor.getAutores());
        view.forward(request, response);
    }
 
}